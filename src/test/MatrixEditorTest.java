package test;

import com.minhaskamal.egamiLight.Matrix;
import com.minhaskamal.egamiLight.MatrixEditor;

public class MatrixEditorTest extends EgamiLightTest{
	public static void main(String[] args) {
		new MatrixEditorTest().runTest();
	}
	
	@Override
	public void testMethod() throws Exception{
		Matrix matrix = new Matrix(IMG, Matrix.RED_GREEN_BLUE);
		
		matrix = MatrixEditor.resize(matrix, 300, 1800);
		
		matrix.write(DESKTOP+"out1.png");
	}
}
