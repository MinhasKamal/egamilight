package test;

import com.minhaskamal.egamiLight.Matrix;
import com.minhaskamal.egamiLight.MatrixMath;
import com.minhaskamal.egamiLight.MatrixUtilities;;

public class MatrixUtilitiesTest extends EgamiLightTest{

	public static void main(String[] args) {
		new MatrixUtilitiesTest().runTest();
	}
	
	@Override
	public void testMethod() throws Exception {
		Matrix matrix = new Matrix(TINY_IMG, Matrix.BLACK_WHITE);
		System.out.println(matrix.toString());
		
		matrix = MatrixMath.multiply(matrix, 5);
		System.out.println(matrix.toString());
		
		matrix = MatrixUtilities.normalizeMatrix(matrix);
		System.out.println(matrix.toString());
	}

}
