package test;

import com.minhaskamal.egamiLight.Matrix;

public class MatrixTest extends EgamiLightTest{
	public static void main(String[] args) {
		new MatrixTest().runTest();
	}
	
	@Override
	public void testMethod() throws Exception{
		Matrix matrix = new Matrix(TINY_IMG, Matrix.RED_GREEN_BLUE);

		System.out.println(matrix.toString());
		matrix = Matrix.load(matrix.dump());
		
		matrix.write(DESKTOP+"out1.png");
	}
}
