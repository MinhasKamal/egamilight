/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Modification Date: 04-Oct-2016                           *
* License: MIT License                                     *
***********************************************************/


package test;

public abstract class EgamiLightTest {
	public static final String DESKTOP = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\",
			RESOURCE = "src\\res\\imgs\\";
	
	public static final String IMG = RESOURCE+"image.png";
	public static final String TINY_IMG = RESOURCE+"tinyImage.png";
	
	public static final String START_MESSAGE = "<--Operation Started-->";
	public static final String SUCCESS_MESSAGE = "<--Operation Successful!-->";

	private long startTime;
	
	
	
	public EgamiLightTest() {
		startTime = 0;
	}
	
	
	
	public void runTest(){
		this.startTime = System.currentTimeMillis();
		
		try {
			System.out.println(START_MESSAGE);
			
			testMethod();
			
			System.out.println(SUCCESS_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			System.out.println("Time Elapsed: " + getElapsedTime() + "s");
		}
	}
	
	public double getElapsedTime(){
		return (double)(System.currentTimeMillis()-startTime)/1000;
	}
	
	public abstract void testMethod() throws Exception;
}
