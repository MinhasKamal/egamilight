package test;

import com.minhaskamal.egamiLight.Matrix;
import com.minhaskamal.egamiLight.MatrixTransformer;

public class MatrixTransformerTest extends EgamiLightTest{

	public static void main(String[] args) {
		new MatrixTransformerTest().runTest();
	}
	
	@Override
	public void testMethod() throws Exception {
		Matrix matrix = new Matrix(IMG, Matrix.RED_GREEN_BLUE);
		
		MatrixTransformer matrixEditor = new MatrixTransformer(4, 5) {
			@Override
			public int editFunction(int pixel) {
				return (int) (constant*pixel/constant2);
			}
		};
		matrix = matrixEditor.edit(matrix);
		
		matrix.write(DESKTOP+"out1.png");
	}

}
