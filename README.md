# :snowman: EgamiLight

[![Gitter](https://badges.gitter.im/MinhasKamal/Egami.svg)](https://gitter.im/MinhasKamal/Egami?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

#### A Light Weight Image Processing Library

### License
<a rel="license" href="https://opensource.org/licenses/MIT"><img alt="MIT License" src="https://cloud.githubusercontent.com/assets/5456665/18950087/fbe0681a-865f-11e6-9552-e59d038d5913.png" width="60em" height=auto/></a><br/><a href="https://github.com/MinhasKamal/EgamiLight">EgamiLight</a> is licensed under <a rel="license" href="https://opensource.org/licenses/MIT">MIT License</a>.
